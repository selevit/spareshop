��    !      $  /   ,      �  <   �     &     *  	   8     B     N     d     j     q     y     �  V   �     �     �     �     �     �            !   +     M     \     i     n     t     {     �  	   �     �  
   �     �  1   �    �  5        F  ,   W  !   �  .   �  ?   �  
              ,     8  
   H  /  S  
   �	     �	     �	     �	  /   �	  '   �	  B   
  F   Y
     �
     �
  
   �
     �
  	   �
     �
  
          .   $  !   S  !   u  _   �                                       !                                                                                                          
      	        
            Page %(page_number)s of %(num_pages)s.
         Add Add new spare Add spare Bike spares Bike's spares catalog Brand Brand: Brand:  Contact: Find Found %(total)s item for query "%(query)s" Found %(total)s items for query "%(query)s" Home Name Name: Next No brands here... No spares found. No spares was added. Phone must contain 5 to 15 digits Phone or email Popularity:  Prev Price Price: Query is empty Search Search... Spares for bike Top brands Top of brands Your query is empty. Please fill the search form. Project-Id-Version: ivi.ru contest
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-09 07:58+0000
PO-Revision-Date: 2016-04-08 22:40+0300
Last-Translator: Sergey Levitin <selevit@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 1.5.4
 
Страница %(page_number)s из %(num_pages)s. Добавить Добавить новую запчасть Добавить запчасть Запчасти для велосипедов Каталог запчестей для велосипедов Бренд Бренд: Бренд: Контакт: Найти Найден %(total)s элемент по запросу «%(query)s» Найдено %(total)s элемента по запросу «%(query)s» Найдено %(total)s элементов по запросу «%(query)s» Найдено %(total)s элементов по запросу «%(query)s» Домой Название Название: Вперед Тут нет ни одного бренда... Запчастей не найдено. Ни одной запчасти не было добавлено. Телефон должен содержать от 5 до 15 цифр Телефон или E-mail Популярность: Назад Цена Цена: Запрос пуст Поиск Поиск... Запчасти для велосипедов Популярные бренды Популярные бренды Ваш запрос пуст. Пожалуйста, заполните форму поиска. 