import json

from django import http
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.core.serializers.json import DjangoJSONEncoder


class JSONResponse(http.HttpResponse):
    """
    Mixin for the rendering of JSON responses
    """

    def __init__(self, content='', encoder_cls=None, *args, **kwargs):
        if 'mimetype' not in kwargs:
            kwargs['content_type'] = 'application/json'
        if not isinstance(content, basestring):
            content = json.dumps(content, cls=encoder_cls)
        super(JSONResponse, self).__init__(content, *args, **kwargs)


class JSONViewMixin(object):
    """
    Mixin for views with a JSON response
    """

    encoder_cls = DjangoJSONEncoder

    def render_to_response(self, *args, **kwargs):
        kwargs['encoder_cls'] = self.encoder_cls
        return JSONResponse(*args, **kwargs)


class CSRFExemptMixin(object):
    """
    Disables CSRF token checking for class-based views
    """

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CSRFExemptMixin, self).dispatch(*args, **kwargs)
