from django.views.generic import ListView
from django.views.generic.edit import CreateView
from django.core.urlresolvers import reverse_lazy
from django.conf import settings

from core import models


class CatalogView(ListView):
    """
    The catalog of spares
    """

    paginate_by = getattr(settings, 'SPARES_PAGINATE_BY', 15)
    model = models.Spare


class SearchView(ListView):
    """
    Search for a spares
    """

    paginate_by = getattr(settings, 'SPARES_PAGINATE_BY', 15)
    model = models.Spare
    template_name = 'core/search.html'

    def get(self, *args, **kwargs):
        query = self.request.GET.get('q', None)
        if query is not None and len(query) > 0:
            self.queryset = self.model.objects.find(query)
        response = super(SearchView, self).get(*args, **kwargs)
        response.context_data['query'] = query
        return response


class AddView(CreateView):
    """
    Add the spare to the catalog
    """

    model = models.Spare
    fields = ('name', 'brand', 'price', 'contact')
    success_url = reverse_lazy('home')


class TopBrandsView(ListView):
    """
    List of the most popular brands
    """

    paginate_by = getattr(settings, 'SPARES_PAGINATE_BY', 15)
    template_name = 'core/top_brand_list.html'
    queryset = models.Spare.top_brands.all()
