from django.conf.urls import url

from core import views, api_views


urlpatterns = [
    url(r'^$', views.CatalogView.as_view(), name='home'),
    url(r'^search/$', views.SearchView.as_view(),
        name='search'),
    url(r'^add/$', views.AddView.as_view(), name='add-spare'),
    url(r'^top-brands/$', views.TopBrandsView.as_view(),
        name='top-brands'),

    url(r'^api/spares/$', api_views.SpareListView.as_view(),
        name='api-spares'),
    url(r'^api/top-brands/$', api_views.TopBrandsView.as_view(),
        name='api-top-brands'),
]
