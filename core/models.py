from __future__ import unicode_literals
from decimal import Decimal

from django.db import models
from django.db.models import Count, Q
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
from django.core.validators import EmailValidator
from django.core.validators import RegexValidator


def contact_validator(value):
    """
    Ensure that the contact contains a phone number or a email
    """

    validate_email = EmailValidator()
    validate_phone = RegexValidator(r'^\d{5,15}$',
                                    _('Phone must contain 5 to 15 digits'),
                                    'phone')
    if '@' in value:
        return validate_email(value)
    return validate_phone(value)


class SpareManager(models.Manager):
    """
    Manager for the spare model
    """

    def find(self, query):
        """
        Find objects by name or brand
        """

        qs = self.get_queryset()
        return qs.filter(Q(name__icontains=query) | Q(brand__icontains=query))


class TopBrandsManager(SpareManager):
    """
    Manager for getting the most popular brands
    """

    THRESHOLD = 5

    def get_queryset(self, *args, **kwargs):
        qs = super(TopBrandsManager, self).get_queryset(*args, **kwargs)
        qs = qs.values('brand').annotate(num_brand=Count('brand')).\
            filter(num_brand__gte=self.THRESHOLD).order_by('-num_brand')
        return qs


class Spare(models.Model):
    """
    The spare for a bike
    """

    name = models.CharField(_('Name'), max_length=200, db_index=True)
    brand = models.CharField(_('Brand'), max_length=50, db_index=True)
    price = models.DecimalField(_('Price'), max_digits=10, decimal_places=2,
                                validators=[MinValueValidator(Decimal(0))])
    contact = models.CharField(_('Phone or email'), max_length=70,
                               validators=[contact_validator])

    objects = SpareManager()
    top_brands = TopBrandsManager()

    def __unicode__(self):
        return unicode(self.name)
