# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-28 17:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Spare',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True,
                                          max_length=200, verbose_name='Name')),
                ('brand', models.CharField(db_index=True,
                                           max_length=50, verbose_name='Brand')),
                ('price', models.DecimalField(decimal_places=2,
                                              max_digits=10, verbose_name='Price')),
                ('contact', models.CharField(
                    max_length=70, verbose_name='Phone or email')),
            ],
        ),
    ]
