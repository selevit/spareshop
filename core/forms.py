from django import forms

from core import models


class SpareForm(forms.ModelForm):
    """
    Form for create/update the spare
    """

    class Meta:
        model = models.Spare
        fields = '__all__'
