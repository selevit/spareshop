from django.views.generic import View
from django.views.generic.list import BaseListView
from django.conf import settings

from core import models
from core import forms
from core.mixins import JSONViewMixin, CSRFExemptMixin


def serialize_spare(model):
    return {
        "id": model.pk,
        "name": model.name,
        "brand": model.brand,
        "price": float(model.price),
        "contact": model.contact,
    }


class APIView(View, JSONViewMixin, CSRFExemptMixin):
    pass


class SpareListView(APIView, BaseListView):
    """
    The catalog of spares
    """

    model = models.Spare
    paginate_by = getattr(settings, 'SPARES_PAGINATE_BY', 15)

    def get(self, *args, **kwargs):
        search_query = self.request.GET.get('search', '')
        if len(search_query) > 0:
            self.queryset = self.model.objects.find(search_query)
        return super(SpareListView, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(SpareListView, self).get_context_data(**kwargs)
        return {
            'list': [serialize_spare(obj) for obj in data['object_list']],
            'page': data['page_obj'].number,
            'total_pages': data['paginator'].num_pages,
            'total_count': data['paginator'].count,
        }

    def post(self, *args, **kwargs):
        form = forms.SpareForm(self.request.POST)
        if not form.is_valid():
            return self.render_to_response(form.errors.as_json(), status=400)
        obj = form.save()
        return self.render_to_response(serialize_spare(obj), status=201)


class TopBrandsView(APIView, BaseListView):
    """
    List of the most popular brands
    """

    queryset = models.Spare.top_brands.all()
    paginate_by = getattr(settings, 'SPARES_PAGINATE_BY', 15)

    def get_context_data(self, **kwargs):
        data = super(TopBrandsView, self).get_context_data(**kwargs)
        return {
            'list': [obj for obj in data['object_list']],
            'page': data['page_obj'].number,
            'total_pages': data['paginator'].num_pages,
            'total_count': data['paginator'].count,
        }
